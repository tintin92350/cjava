#pragma once

// SFML
#include <SFML/Graphics.hpp>

// Window stuff
#include "WindowButton.h"
#include "WindowCaption.h"

// Theme
#include "WindowTheme.h"

/**
 * Window class
 * Based on sfml render window but with customized skin
 */
class Window : public sf::RenderWindow
{
    public:

        Window();
        Window(const sf::VideoMode mode, const char * title);

		/**
		 * Initialise
		 */
		void init();

		/**
		 * Apply a theme
		 */
		void applyTheme(IWindowTheme* theme);

        /**
         * Create the window (init + skin)
         */
        virtual void create(const sf::VideoMode mode, const char * title);

        /**
         * Handle events
         */
        virtual bool pollEvent(sf::Event & event);

		/**
		 * Clear buffer
		 */
		virtual void clear(sf::Color clr = sf::Color(0, 0, 0));

		/**
		 * Draw method
		 */
		virtual void draw(const sf::Drawable& drawable, const sf::RenderStates& states = sf::RenderStates::Default);

		/**
		 * Display window skin and draw
		 */
		virtual void display();

		/**
		 * Minimize the window
		 */
		void minimize();

		/**
		 * Maximize the window
		 */
		void maximize();
		
		/**
		 * Is cursor hover titlebar
		 */
		bool isCursorHoverTitleBar() const;

		/**
		 * Get default view
		 */
		sf::View getDefaultView() const;

		/**
		 * Get context size
		 */
		virtual sf::Vector2u getContextSize() const;

    private:

		bool isMaximized;
		sf::View defaultView;
		sf::View backup;

		/**
		 * Caption to handle motion
		 */
		WindowCaption caption;

		/**
		 * Last size of the window (backup)
		 */
		sf::Vector2u lastSize;

		/**
		 * Last position of the window (backup)
		 */
		sf::Vector2i lastPosition;

		/**
		 * Window theme
		 */
		IWindowTheme* theme;

		/**
		 * Update graphics
		 */
		void updateUI();

        /**
         * Title of the window
         */
        sf::Text title;

		/**
		 * System font for the title
		 */
		sf::Font title_font;

        /**
         * Button for closing the window
         */
        WindowButton closeButton;
        
        /**
         * Button to minimize the window
         */
        WindowButton minimizeButton;
        
        /**
         * Button to maximize/resize the window
         */
        WindowButton maximizeButton;

        /**
         * Background of titlebar
         */
        sf::RectangleShape titlebar_background;

        /**
         * Background of the window
         */
        sf::RectangleShape window_background;

		/**
		 * Close state
		 */
		bool close_state;

		/**
		 * Handle events of the window
		 */
		void processEvents(sf::Event & event);
};