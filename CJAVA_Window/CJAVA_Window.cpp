// CJAVA_Window.cpp : Defines the entry point for the application.
//

#include <SFML/Graphics.hpp>

#include "Window.h"

#include <Windows.h>

#ifdef _DEBUG
int main()
#else
int WINAPI wWinMain(_In_ HINSTANCE hInstance,
					_In_opt_ HINSTANCE hPrevInstance,
					_In_ LPWSTR lpCmdLine,
					_In_ int nShowCmd)
#endif
{
	Window window(sf::VideoMode(960, 540), "DUT INFO - FORUM");

	while (window.isOpen())
	{
		sf::Event e;

		while (window.pollEvent(e))
		{
			if (e.type == sf::Event::Closed) {
				window.close();
			}
		}

		window.clear();

		window.display();
	}

	return 0;
}