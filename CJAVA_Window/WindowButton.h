#pragma once

// STD
#include <functional>

// SFML
#include <SFML/Graphics.hpp>

/**
 * Window Button class
 * Button used by window titlebar
 */
class WindowButton : public sf::Drawable, public sf::Transformable
{
    public:

        WindowButton();
        WindowButton(const sf::Vector2f position, const sf::Color color);

		/**
		 * All state that button can take 
		 */
		enum State {
			NORMAL,
			HOVER,
			CLICKED,
			RELEASED
		};

		/**
		 * Set the color of the shape
		 */
		void setFillColor(const sf::Color color);

		/**
		 * Handle event from user
		 */
		void handleEvents(sf::Event & event);

		/**
		 * Bind a function to the button
		 */
		void bindFunction(std::function<void()> _function);

		/**
		 * Get current state
		 */
		int getCurrentState() const;

    private:

        /**
         * Button shape
         */
        sf::CircleShape shape;

		/**
		 * Create the shape
		 */
		void onCreate();

		/**
		 * Current state of the button
		 */
		int current_state;

		/**
		 * Function bound to this button
		 */
		std::function<void()> _function;

        /**
         * Draw method
         */
        virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const
        {
            states.transform *= this->getTransform();
            target.draw(shape, states);
        }
};