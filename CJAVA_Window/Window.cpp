#include "Window.h"

#include <Windows.h>

Window::Window() :
	caption(this)
{
	this->init();
}

Window::Window(const sf::VideoMode mode, const char* title) :
	caption(this)
{
	this->create(mode, title);
	this->init();
}

void Window::init()
{
	defaultView = sf::RenderWindow::getDefaultView();
	backup = sf::RenderWindow::getDefaultView();

	close_state = false;

	isMaximized = false;
}

void Window::applyTheme(IWindowTheme* theme)
{
	this->theme = theme;

	titlebar_background.setFillColor(this->theme->WINDOW_CAPTION());
	closeButton.setFillColor(this->theme->WINDOW_CLOSE_BUTTON());
	minimizeButton.setFillColor(this->theme->WINDOW_MINIMIZE_BUTTON());
	maximizeButton.setFillColor(this->theme->WINDOW_MAXIMIZE_BUTTON());
	titlebar_background.setOutlineColor(this->theme->WINDOW_CAPTION_BORDER_COLOR());
	titlebar_background.setOutlineThickness(this->theme->WINDOW_CAPTION_BORDER_THICKNESS());
	title.setFillColor(this->theme->WINDOW_CAPTION_TITLE_COLOR());
}

void Window::create(const sf::VideoMode mode, const char* title)
{
	sf::ContextSettings settings(24);
	settings.antialiasingLevel = 4;

	sf::VideoMode modeExt = mode;
	modeExt.height += mode.height == sf::VideoMode::getDesktopMode().height ? 0 : 32;

	sf::RenderWindow::create(modeExt, title, sf::Style::None, settings);

	float titlebar_height = 32.0f;

	titlebar_background.setSize(sf::Vector2f(mode.width, titlebar_height));

	closeButton.setPosition(12.0f, 10.0f);
	closeButton.bindFunction([this] {this->close_state = true; });

	minimizeButton.setPosition(32.f, 10.0f);
	minimizeButton.bindFunction(std::bind(&Window::minimize, this));

	maximizeButton.setPosition(52.0f, 10.0f);
	maximizeButton.bindFunction(std::bind(&Window::maximize, this));

	title_font.loadFromFile("C:/Windows/Fonts/SairaCondensed-Bold.ttf");

	this->title = sf::Text(title, title_font, 16);
	sf::FloatRect rect = this->title.getLocalBounds();
	this->title.setOrigin(roundf(rect.left + rect.width / 2.0f), roundf(rect.top + rect.height / 2.0f));
	this->title.setPosition((float)modeExt.width / 2.0f, 16.0f);

	HRGN RoundedWindowArea = CreateRoundRectRgn(0, 0, modeExt.width, modeExt.height, 5, 5);
		SetWindowRgn(this->getSystemHandle(), RoundedWindowArea, TRUE);

	applyTheme(new DefaultWindowThemeDark());

	lastSize.x = modeExt.width;
	lastSize.y = modeExt.height;

	lastPosition = this->getPosition();

	caption.setZone(sf::IntRect(0, 0, modeExt.width, 32));
}

bool Window::pollEvent(sf::Event& event)
{
	bool keep = sf::RenderWindow::pollEvent(event);

	if (close_state) {
		event.type = sf::Event::Closed;
		close_state = false;
		return true;
	}

	processEvents(event);

	return keep;
}

void Window::clear(sf::Color clr)
{
	sf::RenderWindow::clear(theme ? this->theme->WINDOW_BACKGROUND() : clr);
}

void Window::draw(const sf::Drawable& drawable, const sf::RenderStates& states)
{
	sf::RenderStates nstates = states;
	nstates.transform.translate(sf::Vector2f(0.0f, 32.0f));

	sf::RenderWindow::draw(drawable, nstates);
}

void Window::display()
{
	sf::View view_backup = this->getView();
	this->setView(defaultView);

	sf::RenderWindow::draw(titlebar_background);
	sf::RenderWindow::draw(title);
	sf::RenderWindow::draw(closeButton);
	sf::RenderWindow::draw(minimizeButton);
	sf::RenderWindow::draw(maximizeButton);

	this->setView(view_backup);

	sf::RenderWindow::display();
}

void Window::minimize()
{
	HWND hWnd = this->getSystemHandle(); //What do I add here to minimize the window??
	ShowWindow(hWnd, SW_MINIMIZE);
}

void Window::maximize()
{
	isMaximized = !isMaximized;

	if (isMaximized)
	{
		this->setSize(sf::Vector2u(sf::VideoMode::getDesktopMode().width, sf::VideoMode::getDesktopMode().height));
		this->setPosition(sf::Vector2i(0, 0));

		defaultView = sf::View(sf::Vector2f(sf::VideoMode::getDesktopMode().width / 2.0f, sf::VideoMode::getDesktopMode().height / 2.0f), sf::Vector2f(sf::VideoMode::getDesktopMode().width, sf::VideoMode::getDesktopMode().height));
	}
	else
	{
		this->setSize(lastSize);
		this->setPosition(lastPosition);

		defaultView = backup;
	}

	updateUI();
}

bool Window::isCursorHoverTitleBar() const
{
	sf::Vector2f cursor(sf::Mouse::getPosition(*this));

	return cursor.y <= 32.0f;
}

sf::View Window::getDefaultView() const
{
	return defaultView;
}

sf::Vector2u Window::getContextSize() const
{
	return sf::RenderWindow::getSize() + sf::Vector2u(0, -32);
}

void Window::updateUI()
{
	titlebar_background.setSize(sf::Vector2f(defaultView.getSize().x, 32.0f));

	sf::FloatRect rect = this->title.getLocalBounds();
	this->title.setOrigin(roundf(rect.left + rect.width / 2.0f), roundf(rect.top + rect.height / 2.0f));
	this->title.setPosition((float)defaultView.getSize().x / 2.0f, 16.0f);
}

void Window::processEvents(sf::Event & event)
{
	closeButton.handleEvents(event);
	minimizeButton.handleEvents(event);
	maximizeButton.handleEvents(event);

	if (closeButton.getCurrentState()		== WindowButton::State::NORMAL	&&
		minimizeButton.getCurrentState()	== WindowButton::State::NORMAL	&&
		maximizeButton.getCurrentState()	== WindowButton::State::NORMAL	&&
		!isMaximized)
	{
		caption.drag(event);
	}
}
