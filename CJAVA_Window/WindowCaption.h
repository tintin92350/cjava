#pragma once

class Window;

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Rect.hpp>

class WindowCaption
{
	public:

		WindowCaption(Window* parent);

		/**
		 * Returns true if the window is currenty
		 * dragged by the user
		 */
		bool isDragged() const;

		/**
		 * Drag method that allow the window
		 * to move around screen
		 */
		void drag(const sf::Event e);

		/**
		 * Set active zone where can be applied rules.
		 * Where cursor need to be to drag the window
		 */
		void setZone(const sf::IntRect zone);

		/**
		 * Returns the zone where drag is effective
		 */
		sf::IntRect getZone() const;

	private:

		bool dragged;
		sf::Vector2i dragLocation;
		Window* parent;
		sf::IntRect zone;
};
