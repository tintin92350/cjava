#pragma once

// Events that can produce a window
enum WindowEventType {
    CLOSE,
    MOVE,
    RESIZE,
    MINIMIZE,
    MAXIMIZE
};