#include "EventObject.h"

// IllegalArgumentException
#include "../lang/IllegalArgumentException.h"

using namespace lang;
using namespace util;

util::EventObject::EventObject(Object::PTR source)
{
	if (source == nullptr || !source)
		throw new IllegalArgumentException("Source is empty");

    this->source = source;
}

Object::PTR util::EventObject::getSource()
{
	return source;
}

std::string util::EventObject::toString() const
{
	return std::string("<EventObject> [" + std::to_string(hashCode()) + "]");
}
