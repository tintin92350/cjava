#pragma once

// STD
#include <string>

// Object
#include "../lang/Object.h"

namespace util
{
	/**
     * The root class from which all event state objects shall be derived.
     * 
     * All Events are constructed with a reference to the object, the "source", 
     * that is logically deemed to be the object upon which the Event in 
     * question initially occurred upon.
     */
	class EventObject : public lang::Object
	{
		public:

			/**
			 * Default constructor
			 */
			EventObject(lang::Object::PTR source);

            /**
             * Returns the object on which the Event initially occurred.
             */
			lang::Object::PTR getSource();
            
			/**
			 * Returns a string representation of the object.
			 */
			virtual std::string toString() const;

        protected:

            /**
             * The object on which the Event initially occurred.
             */
			lang::Object::PTR source;
	};
}