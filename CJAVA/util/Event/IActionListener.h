#pragma once

// Focus event
#include "../FocusEvent.h"

// Action event
#include "../ActionEvent.h"

/**
 * The listener interface for receiving action events. The class that 
 * is interested in processing an action event implements this interface, 
 * and the object created with that class is registered with a component, 
 * using the component's addActionListener method. When the action event 
 * occurs, that object's actionPerformed method is invoked.
 */
__interface IActionListener
{
	public:
	
        /**
         * Invoked when a component gains the keyboard focus.
         */
		virtual void actionPerformed(util::ActionEvent e);

};