	#pragma once

// KeyEvent
#include "../KeyEvent.h"

/**
 * The listener interface for receiving keyboard events (keystrokes). The class 
 * that is interested in processing a keyboard event either implements this 
 * interface (and all the methods it contains) or extends the abstract 
 * KeyAdapter class (overriding only the methods of interest). 
 */
__interface IKeyListener
{
	public:
    
        /**
         * Invoked when a key has been typed. See the class description for 
         * KeyEvent for a definition of a key typed event.
         */
		virtual void KeyPressed(util::KeyEvent e)     = 0;
        
        /**
         * Invoked when a key has been pressed. See the class description for   
         * KeyEvent for a definition of a key pressed event.
         */
		virtual void KeyReleased(util::KeyEvent e)    = 0;

        /**
         * Invoked when a key has been released. See the class description for 
         * KeyEvent for a definition of a key released event.
         */
		virtual void KeyTyped(util::KeyEvent e)       = 0;
};