#include "ActionEvent.h"

util::ActionEvent::ActionEvent(lang::Object::PTR source, std::string command) :
	EventObject(source)
{
	this->command = command;
}

std::string util::ActionEvent::getActionCommand() const
{
	return command;
}
