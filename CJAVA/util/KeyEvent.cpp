#include "KeyEvent.h"


util::KeyEvent::KeyEvent(lang::Object::PTR source, int keycode):
	EventObject(source)
{
	setKeyCode(keycode);
}

util::KeyEvent::KeyEvent(lang::Object::PTR source, int keycode, char keychar) :
	EventObject(source)
{
	setKeyChar(keycode);
	setKeyChar(keychar);
}

int util::KeyEvent::getKeyCode() const
{
	return KeyCode;
}

char util::KeyEvent::getKeyChar() const
{
	return KeyChar;
}

void util::KeyEvent::setKeyChar(char keyChar)
{
	KeyChar = keyChar;
}

void util::KeyEvent::setKeyCode(int keyCode)
{
	KeyCode = keyCode;
}
