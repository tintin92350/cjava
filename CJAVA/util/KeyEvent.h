#pragma once

// STD
#include <string>

// Object
#include "../lang/Object.h"
#include "EventObject.h"

namespace util
{
	/**
     * An event which indicates that a keystroke occurred in a component.  
     */
	class KeyEvent : public util::EventObject
	{
		public:

			/**
			 * Default constructor
			 */
			KeyEvent(lang::Object::PTR source, int keycode);
			KeyEvent(lang::Object::PTR source, int keycode, char keychar);

            /**
             * Returns the integer keyCode associated with the key in this 
             * event.
             */
            int getKeyCode() const;
            
            /**
             * Returns the integer character associated with the key in this 
             * event.
             */
            char getKeyChar() const;

            /**
             * Set the keyChar value to indicate a logical character.
             */
            void setKeyChar(char keyChar);

            /**
             * Set the keyCode value to indicate a physical key..
             */
            void setKeyCode(int keyCode);

        private:

            int KeyCode;
            char KeyChar;
	};
}