#pragma once

// STD
#include <string>

// Object
#include "Object.h"

// Exception
#include "Exception.h"

namespace lang
{
	/**
     * The class Exception and its subclasses are a form of Throwable that 
     * indicates conditions that a reasonable application might want to catch.
     * 
     * The class Exception and any subclasses that are not also subclasses of 
     * RuntimeException are checked exceptions. Checked exceptions need to be 
     * declared in a method or constructor's throws clause if they can be 
     * thrown by the execution of the method or constructor and propagate 
     * outside the method or constructor boundary. 
     */
	class IllegalArgumentException : public lang::Exception
	{
		public:

			/**
			 * Default constructor
			 */
			IllegalArgumentException();

			/**
			 * Constructors
			 */
			IllegalArgumentException(std::string message);
			IllegalArgumentException(std::string message, Throwable * cause);
			IllegalArgumentException(std::string message, Throwable * cause, bool enableSupression, bool writableStackTrace);
			IllegalArgumentException(Throwable * cause);

			/**
			 * Returns a string representation of the object.
			 */
			virtual std::string toString() const;

        protected:
	};
}