#pragma once

// STD
#include <string>
#include <memory>

namespace lang
{
	/**
	 * Class Object is the root of the class hierarchy. Every class has Object 
	 * as a superclass. All objects, including arrays, implement the methods of 
	 * this class.
	 */
	class Object
	{
		public:

			/**
			 * Shared pointer type
			 */
			typedef std::shared_ptr<Object> PTR;

			/**
			 * Default constructor
			 */
			Object();

			/**
			 * Copy constructor
			 */
			Object(const Object * object);

			/**
			 * Creates and returns a copy of this object.
			 */
			virtual Object::PTR clone() ;

			/**
			 * Indicates whether some other object is "equal to" this one
			 */
			bool equals(const Object* obj) const;

			/**
			 * Returns a hash code value for the object.
			 */
			virtual int hashCode() const;

			/**
			 * Returns a string representation of the object.
			 */
			virtual std::string toString() const;
	};

	/**
	 * Indicates whether some other object is "equal to" this one
	 */
	inline bool operator==(const Object& left, const Object& right)
	{
		return left.equals(&right);
	}

	/**
	 * Indicates whether some other object is not "equal to" this one
	 */
	inline bool operator!=(const Object& left, const Object& right)
	{
		return !left.equals(&right);
	}
}