#include "Exception.h"

lang::Exception::Exception() : 
	Throwable()
{
}

lang::Exception::Exception(std::string message) : 
	Throwable(message)
{
}

lang::Exception::Exception(std::string message, Throwable* cause) : 
	Throwable(message, cause)
{
}

lang::Exception::Exception(std::string message, Throwable* cause, bool enableSupression, bool writableStackTrace) :
	Throwable(message, cause, enableSupression, writableStackTrace)
{
}

lang::Exception::Exception(Throwable* cause) :
	Throwable(cause)
{
}

std::string lang::Exception::toString() const
{
	return std::string("<Exception> [" + std::to_string(hashCode()) + "]");
}
