#include "Object.h"

using namespace lang;

/**
 * Default constructor
 * Generate a new serial ID for the object
 */
Object::Object()
{
}

/**
 * Copy constructor
 */
Object::Object(const Object * object)
{
	*this = *object;
}

/**
 * Creates and returns a copy of this object.
 */
Object::PTR Object::clone()
{
	return std::make_shared<Object>(new Object(this));
}

/**
 * Indicates whether some other object is "equal to" this one
 * Two object are equals only if they have the same serial ID
 */
bool Object::equals(const Object* obj) const
{
	return obj->hashCode() == hashCode();
}

/**
 * Returns a hash code value for the object.
 */
int Object::hashCode() const
{
	return reinterpret_cast<int>(this);
}

/**
 * Returns a string representation of the object.
 */
std::string Object::toString() const
{
	return std::string("<Object> [" + std::to_string(hashCode()) + "]");
}
