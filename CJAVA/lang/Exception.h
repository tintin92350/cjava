#pragma once

// STD
#include <string>

// Object
#include "Object.h"

// Throwable
#include "throwable.h"

namespace lang
{
	/**
     * The class Exception and its subclasses are a form of Throwable that 
     * indicates conditions that a reasonable application might want to catch.
     * 
     * The class Exception and any subclasses that are not also subclasses of 
     * RuntimeException are checked exceptions. Checked exceptions need to be 
     * declared in a method or constructor's throws clause if they can be 
     * thrown by the execution of the method or constructor and propagate 
     * outside the method or constructor boundary. 
     */
	class Exception : public lang::Throwable, public std::exception
	{
		public:

			/**
			 * Default constructor
			 */
			Exception();

			/**
			 * Constructors
			 */
			Exception(std::string message);
			Exception(std::string message, Throwable * cause);
			Exception(std::string message, Throwable * cause, bool enableSupression, bool writableStackTrace);
			Exception(Throwable * cause);

			/**
			 * Returns a string representation of the object.
			 */
			virtual std::string toString() const;

        protected:
	};
}