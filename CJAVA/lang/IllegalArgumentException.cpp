#include "IllegalArgumentException.h"

lang::IllegalArgumentException::IllegalArgumentException() :
	Exception()
{
	this->message = "Exception : Illegal Argument";
}

lang::IllegalArgumentException::IllegalArgumentException(std::string message) :
	Exception("Exception : Illegal Argument" +message)
{
}

lang::IllegalArgumentException::IllegalArgumentException(std::string message, Throwable* cause) :
	Exception("Exception : Illegal Argument"+message, cause)
{
}

lang::IllegalArgumentException::IllegalArgumentException(std::string message, Throwable* cause, bool enableSupression, bool writableStackTrace) :
	Exception("Exception : Illegal Argument"+message, cause, enableSupression, writableStackTrace)
{
}

lang::IllegalArgumentException::IllegalArgumentException(Throwable* cause) :
	Exception(cause)
{
}

std::string lang::IllegalArgumentException::toString() const
{
	return std::string("<Exception|IllegalArgumentException> [" + std::to_string(hashCode()) + "]");
}
