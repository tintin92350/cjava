#include "throwable.h"

lang::Throwable::Throwable()
{
	this->message = "";
	this->cause   = nullptr;

	this->enableSupression		= false;
	this->writableStackTrace	= false;
}

lang::Throwable::Throwable(std::string message)
{
	this->message = message;
	this->cause   = nullptr;

	this->enableSupression		= false;
	this->writableStackTrace	= false;
}

lang::Throwable::Throwable(std::string message, Throwable* cause)
{
	this->message = message;
	this->cause   = cause;

	this->enableSupression		= false;
	this->writableStackTrace	= false;
}

lang::Throwable::Throwable(std::string message, Throwable* cause, bool enableSupression, bool writableStackTrace)
{
	this->message = message;
	this->cause   = cause;

	this->enableSupression		= enableSupression;
	this->writableStackTrace	= writableStackTrace;
}

lang::Throwable::Throwable(Throwable* cause)
{
	this->message	= cause ? cause->toString() : "";
	this->cause		= cause;

	this->enableSupression		= false;
	this->writableStackTrace	= false;
}

std::string lang::Throwable::getMessage() const noexcept
{
	return message;
}

std::string lang::Throwable::getLocalizedMessage() const noexcept
{
	return message;
}

lang::Throwable* lang::Throwable::getCause()
{
	return cause;
}

lang::Throwable* lang::Throwable::initCause(Throwable * cause)
{
	return cause;
}

void lang::Throwable::printStackTrace()
{
	printf("%s\n", this->message.c_str());
}

std::string lang::Throwable::toString() const
{
	return std::string("<Throwable> [" + std::to_string(hashCode()) + "]");
}
