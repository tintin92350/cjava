#pragma once

// STD
#include <string>

// Object
#include "Object.h"

namespace lang
{
	/**
     * The Throwable class is the superclass of all errors and exceptions in t
     * he Java language. Only objects that are instances of this class (or one 
     * of its subclasses) are thrown by the Java Virtual Machine or can be 
     * thrown by the Java throw statement. Similarly, only this class or one of 
     * its subclasses can be the argument type in a catch clause. For the 
     * purposes of compile-time checking of exceptions, Throwable and any 
     * subclass of Throwable that is not also a subclass of either 
     * RuntimeException or Error are regarded as checked exceptions. 
     */
	class Throwable : public lang::Object
	{
		public:

			/**
			 * Default constructor
			 */
			Throwable();

			/**
			 * Constructors
			 */
			Throwable(std::string message);
			Throwable(std::string message, Throwable * cause);
			Throwable(std::string message, Throwable * cause, bool enableSupression, bool writableStackTrace);
			Throwable(Throwable * cause);
            
            /**
             * Returns the detail message string of this throwable.
             */
            std::string getMessage() const noexcept;
            
            /**
             * Creates a localized description of this throwable. 
             * Subclasses may override this method in order to produce a 
             * locale-specific message. For subclasses that do not override 
             * this method, the default implementation returns the same result 
             * as getMessage().
             */
            virtual std::string getLocalizedMessage() const noexcept;

            /**
             * Returns the cause of this throwable or null if the cause is 
             * nonexistent or unknown. (The cause is the throwable that caused 
             * this throwable to get thrown.) 
             * 
             * This implementation returns the cause that was supplied via one 
             * of the constructors requiring a Throwable, or that was set after 
             * creation with the initCause(Throwable) method. While it is 
             * typically unnecessary to override this method, a subclass can 
             * override it to return a cause set by some other means. This is 
             * appropriate for a "legacy chained throwable" that predates the 
             * addition of chained exceptions to Throwable. Note that it is not 
             * necessary to override any of the PrintStackTrace methods, all of 
             * which invoke the getCause method to determine the cause of a 
             * throwable.
             */
            Throwable * getCause();

            /**
             * Initializes the cause of this throwable to the specified value. 
             * (The cause is the throwable that caused this throwable to get 
             * thrown.)
             * 
             * This method can be called at most once. It is generally called 
             * from within the constructor, or immediately after creating the 
             * throwable. If this throwable was created with Throwable
             * (Throwable) or Throwable(String,Throwable), this method cannot 
             * be called even once. 
             */
            Throwable * initCause(Throwable * cause);

            /**
             * Prints this throwable and its backtrace to the standard error 
             * stream. This method prints a stack trace for this Throwable 
             * object on the error output stream that is the value of the field 
             * System.err. The first line of output contains the result of the 
             * toString() method for this object. Remaining lines represent 
             * data previously recorded by the method fillInStackTrace(). The 
             * format of this information depends on the implementation, but 
             * the following example may be regarded as typical: 
             */
            void printStackTrace();

			/**
			 * Returns a string representation of the object.
			 */
			virtual std::string toString() const;

        protected:

            /**
             * detail message string of this throwable
             */
			std::string message;

			/**
			 * the cause of this throwable
			 */
			Throwable * cause;

			/**
             * Is supression enabled
             */
            bool enableSupression;
            
			/**
             * Can we write trace
             */
            bool writableStackTrace;
	};
}