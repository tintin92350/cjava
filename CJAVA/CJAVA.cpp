// CJAVA.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#include "lang/Object.h"
#include "lang/Exception.h"
#include "lang/IllegalArgumentException.h"
#include "util/EventObject.h"

#include "util/Event/IActionListener.h"
#include "util/Event/IKeyListener.h"
#include "util/ActionEvent.h"

#include "awt/Component.h"
#include "awt/Window.h"
#include "awt/FlowLayout.h"
#include "awt/GridLayout.h"
#include "awt/FreeWayLayout.h"
#include "awt/Panel.h"

#include "awt/Components/Button.h"
#include "awt/Components/TextField.h"
#include "awt/Components/Label.h"
#include "awt/Components/TextArea.h"

class ControllerTest : public IActionListener, public IKeyListener
{
	awt::TextField::PTR name;
	awt::TextField::PTR nickname;
	awt::TextArea::PTR output;

	public:

		ControllerTest(awt::TextField::PTR name, awt::TextField::PTR nickname, awt::TextArea::PTR output)
		{
			this->name = name;
			this->nickname = nickname;
			this->output = output;
		}

		void actionPerformed(util::ActionEvent e)
		{
			output->append(name->getString() + " AKA " + nickname->getString(), true);
			name->setString("");
			nickname->setString("");
		}

		/**
		 * Invoked when a key has been typed. See the class description for
		 * KeyEvent for a definition of a key typed event.
		 */
		virtual void KeyPressed(util::KeyEvent e)
		{
		}

		/**
		 * Invoked when a key has been pressed. See the class description for
		 * KeyEvent for a definition of a key pressed event.
		 */
		virtual void KeyReleased(util::KeyEvent e) {

		}

		/**
		 * Invoked when a key has been released. See the class description for
		 * KeyEvent for a definition of a key released event.
		 */
		virtual void KeyTyped(util::KeyEvent e) {
		}
};

class myPanel : public awt::Panel
{
	private:

		awt::Label::PTR name;
		awt::TextField::PTR name_input;

		awt::Label::PTR nickname;
		awt::TextField::PTR nickname_input;

		awt::Button::PTR print_btn;

		awt::TextArea::PTR output_textarea;

		std::shared_ptr<ControllerTest> controler;

	public:

		myPanel()
		{
			setLayout(new awt::FreeWayLayout());

			name = awt::Label::create(new awt::Label("Enter your name"));
			name_input = awt::TextField::create(new awt::TextField());
			name->setLocation(10, 10);
			name_input->setLocation(200, 10);

			nickname = awt::Label::create(new awt::Label("Enter your nick name"));
			nickname_input = awt::TextField::create(new awt::TextField());
			nickname->setLocation(10, 35);
			nickname_input->setLocation(200, 35);

			output_textarea = awt::TextArea::create(new awt::TextArea());
			output_textarea->setSize(345, 68);
			output_textarea->setLocation(nickname_input->getLocation().getX() + nickname_input->getSize().getWidth() + 10, 10);
			output_textarea->setFont(new awt::Font("fonts/LucidaGrande.ttf", awt::Font::TYPE::PLAIN, 10));

			controler = std::make_shared<ControllerTest>(name_input, nickname_input, output_textarea);

			name_input->addKeyListener(controler.get());

			print_btn = awt::Button::create(new awt::Button("print on creen"));
			print_btn->setSize(awt::Dimension<int>(200 + nickname_input->getSize().getWidth() - 10, 18));
			print_btn->setLocation(10, 60);
			print_btn->addActionListener(controler.get());

			add(name.get());
			add(name_input.get());
			add(nickname.get());
			add(nickname_input.get());
			add(print_btn.get());
			add(output_textarea.get());
		}

		~myPanel()
		{
		}
};

int main()
{	
	awt::Window win("CJAVA");
	win.setSize(awt::Dimension<int>(720, 88));
	win.applyTheme(new DefaultWindowThemeLight());

	awt::Panel* contentPane = new myPanel();

	win.setContentPane(contentPane);
	win.run();

	return 0;
}