#pragma once

#include <vector>

#include "../lang/Object.h"

#include "Point.h"
#include "Dimension.h"
#include "Component.h"
#include "Insets.h"

#include "LayoutManager.h"

namespace awt
{
	/**
	 * The GridLayout class is a layout manager that lays out a container's 
	 * components in a rectangular grid. The container is divided into 
	 * equal-sized rectangles, and one component is placed in each rectangle. 
	 */
	class GridLayout : public lang::Object, public ILayoutManager
	{
		public:

			/**
			 * Creates a grid layout with a default of one column per component, 
			 * in a single row.
			 */
			GridLayout();

			/**
			 * Creates a grid layout with the specified number of rows and columns.
			 */
			GridLayout(int rows, int cols);

			/**
			 * Creates a grid layout with the specified number of rows and columns.
			 */
			GridLayout(int rows, int cols, int hgap, int vgap);

			/**
			 * Lays out the specified container.
			 */
			virtual void layoutContainer(awt::Container* container);

			/**
			 * Calculates the minimum size dimensions for the 
			 * specified container, given the components it contains.
			 */
			virtual Dimension<int> minimumLayoutSize(Container* parent);

			/**
			 * Calculates the preferred size dimensions for the 
			 * specified container, given the components it contains.
			 */
			virtual Dimension<int> preferredLayoutSize(Container *parent);

		protected:

			/**
			 * horizontal and vertical gaps
			 */
			int hgap;
			int vgap;
			int rows;
			int cols;

	};
}