#pragma once

// SFML
#include <SFML/Graphics.hpp>

// Window stuff
#include "WindowButton.h"
#include "WindowCaption.h"

// Theme
#include "WindowTheme.h"

// AWT
#include "Container.h"

namespace awt
{
	/**
	 * Window class
	 * Based on sfml render window but with customized skin
	 */
	class Window : public awt::Container
	{
		public:

			Window();
			Window(const char * title);

			/**
			 * Initialise
			 */
			void init();

			/**
			 * Apply a theme
			 */
			void applyTheme(IWindowTheme* theme);

			/**
			 * Create the window (init + skin)
			 */
			virtual void create(const char * title);

			/**
			 * Do loop
			 */
			void run();

			/**
			 * Handle events
			 */
			virtual bool handleEvents();

			/**
			 * Clear buffer
			 */
			virtual void clear(sf::Color clr = sf::Color(0, 0, 0));

			/**
			 * Draw method
			 */
			virtual void draw(const sf::Drawable& drawable, const sf::RenderStates& states = sf::RenderStates::Default);

			/**
			 * Display window skin and draw
			 */
			virtual void display();

			/**
			 * Minimize the window
			 */
			void minimize();

			/**
			 * Maximize the window
			 */
			void maximize();
		
			/**
			 * Close the window
			 */
			void close();

			/**
			 * Is cursor hover titlebar
			 */
			bool isCursorHoverTitleBar() const;

			/**
			 * Get default view
			 */
			sf::View getDefaultView() const;

			/**
			 * Get context size
			 */
			virtual sf::Vector2u getContextSize() const;

			/**
			 * Update this component
			 */
			virtual void update();

			/**
			 * Is window still open
			 */
			virtual bool isOpen() const;

			/**
			 * Returns a string representation of the object.
			 */
			virtual std::string toString() const;

			/**
			 * Returns the position of the mouse pointer in this Component's coordinate space
			 * if the Component is directly under the mouse pointer, otherwise returns null.
			 */
			virtual Point<int> getMousePosition();

			/**
			 * Sets the contentPane property
			 */
			void setContentPane(Container* container);

		private:

			bool isMaximized;
			sf::View defaultView;
			sf::View backup;

			/**
			 * Content pane
			 */
			Container::PTR contentPane;

			/**
			 * SFML Window handle
			 */
			sf::RenderWindow windowHandle;

			/**
			 * Caption to handle motion
			 */
			WindowCaption caption;

			/**
			 * Last size of the window (backup)
			 */
			awt::Dimension<int> lastSize;

			/**
			 * Last position of the window (backup)
			 */
			awt::Point<int> lastPosition;

			/**
			 * Window theme
			 */
			IWindowTheme* theme;

			/**
			 * Title of the window
			 */
			sf::Text title;

			/**
			 * Button for closing the window
			 */
			WindowButton closeButton;
        
			/**
			 * Button to minimize the window
			 */
			WindowButton minimizeButton;
        
			/**
			 * Button to maximize/resize the window
			 */
			WindowButton maximizeButton;

			/**
			 * Background of titlebar
			 */
			sf::RectangleShape titlebar_background;
			sf::VertexArray titlebar_background_a;

			/**
			 * Background of the window
			 */
			sf::RectangleShape window_background;

			/**
			 * Close state
			 */
			bool close_state;

			/**
			 * Handle events of the window
			 */
			void processEvents(sf::Event & event);
	};
}