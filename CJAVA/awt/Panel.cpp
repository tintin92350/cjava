#include "Panel.h"

#include <iostream>

#include "Container.h"
#include "Window.h"
#include "FlowLayout.h"

awt::Panel::Panel() : 
	window(nullptr)
{
	this->setLayout(new FlowLayout());
}

awt::Panel::Panel(ILayoutManager * layout) : 
	window(nullptr)
{
	this->setLayout(layout);
}

void awt::Panel::setWindowParent(Window* win)
{
	window = win;
}

awt::Window* awt::Panel::getWindowParent()
{
	return window;
}

void awt::Panel::update()
{
	getLayout()->layoutContainer(this);

	for (auto& component : getComponents())
		component->update();
}

void awt::Panel::updateForced()
{
	getLayout()->layoutContainer(this);

	for (size_t i = 0; i < getComponentCount(); i++)
		Component* component = getComponent(i);
}

void awt::Panel::paint(sf::RenderTarget& target, sf::RenderStates states)
{
	states.transform.translate(static_cast<float>(getLocation().getX()), static_cast<float>(getLocation().getY()));

	for (auto& component : getComponents())
		component->paint(target, states);
}

void awt::Panel::handleEventFromUser(const sf::Event& e)
{
	for (auto& component : getComponents()) {
		component->handleEventFromUser(e);
	}
}

awt::Point<int> awt::Panel::getMousePosition()
{
	return getParent()->getMousePosition();
}
