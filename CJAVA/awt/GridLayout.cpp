#include "GridLayout.h"

#include "Container.h"

awt::GridLayout::GridLayout()
{
	this->vgap = 5;
	this->hgap = 5;
	this->rows = 1;
	this->cols = 0;
}

awt::GridLayout::GridLayout(int rows, int cols)
{
	this->vgap = 5;
	this->hgap = 5;
	this->rows = rows;
	this->cols = cols;
}

awt::GridLayout::GridLayout(int rows, int cols, int hgap, int vgap)
{
	this->vgap = vgap;
	this->hgap = hgap;
	this->rows = rows;
	this->cols = cols;
}

void awt::GridLayout::layoutContainer(awt::Container* container)
{
	if (cols == 0)
		cols = container->getComponentCount();
	float individual_width  = (float)container->getSize().getWidth()  / (float)cols;
	float individual_height = (float)container->getSize().getHeight() / (float)rows;

	int x = 0;
	int y = 0;

	for (auto& component : container->getComponents())
	{
		component->setSize(awt::Dimension<int>(static_cast<int>(individual_width), static_cast<int>(individual_height)));
		component->setLocation(awt::Point<int>(static_cast<int>(x * individual_width + hgap), static_cast<int>(y * individual_height + vgap)));

		x++;

		if (x >= cols)
		{
			y++;
			x = 0;
		}
	}
}

awt::Dimension<int> awt::GridLayout::minimumLayoutSize(Container* parent)
{
	return Dimension<int>();
}

awt::Dimension<int> awt::GridLayout::preferredLayoutSize(Container* parent)
{
	return Dimension<int>();
}
