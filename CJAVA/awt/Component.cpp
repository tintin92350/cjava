#include "Component.h"

#include "Container.h"

awt::Component::Component() :
	need_update(true),
	parent(nullptr),
	font(nullptr)
{
}

awt::Component::~Component()
{
	if(font && font != nullptr)
		delete font;
}

awt::Point<int> awt::Component::getLocation() const
{
	return location;
}

awt::Point<int> awt::Component::getLocation(Point<int>& rv)
{
	rv = *std::dynamic_pointer_cast<awt::Point<int>>(location.clone());
	return Point<int>();
}

void awt::Component::setLocation(const int x, const int y)
{
	location = Point<int>(x, y);
	need_update = true;
}

void awt::Component::setLocation(const Point<int> p)
{
	location = p;
	need_update = true;
}

awt::Dimension<int> awt::Component::getMaximumSize() const
{
	return maximumSize;
}

void awt::Component::setMaximumSize(const Dimension<int> maximumSize)
{
	this->maximumSize = maximumSize;
	need_update = true;
}

awt::Dimension<int> awt::Component::getMinimumSize() const
{
	return minimumSize;
}

void awt::Component::setMinimumSize(const Dimension<int> minimumSize)
{
	this->minimumSize = minimumSize;
	need_update = true;
}

awt::Dimension<int> awt::Component::getPrefferedSize() const
{
	return prefferedSize;
}

void awt::Component::setPrefferedSize(const Dimension<int> prefferedSize)
{
	this->prefferedSize = prefferedSize;
	need_update = true;
}

awt::Dimension<int> awt::Component::getSize() const
{
	return size;
}

void awt::Component::setSize(const Dimension<int> size)
{
	this->size = size;
	need_update = true;
}

void awt::Component::setSize(const int w, const int h)
{
	this->size = Dimension<int>(w, h);
	need_update = true;
}

void awt::Component::update()
{
}

void awt::Component::updateForced()
{
	need_update = true;
	this->update();
}

void awt::Component::paint(sf::RenderTarget& target, sf::RenderStates states)
{
}

void awt::Component::handleEventFromUser(const sf::Event& e)
{
}

void awt::Component::setParent(Container* p)
{
	this->parent = p;
}

awt::Container* awt::Component::getParent()
{
	return parent;
}

awt::Point<int> awt::Component::getMousePosition()
{
	return Point<int>();
}

void awt::Component::setFont(Font* font)
{
	if (this->font != nullptr)
		delete this->font;

	this->font = std::move(font);
	need_update = true;
}

awt::Font* awt::Component::getFont()
{
	return font;
}

void awt::Component::setForeground(const sf::Color color)
{
	foreground = color;
	need_update = true;
}

sf::Color awt::Component::getForeground() const
{
	return foreground;
}

void awt::Component::setBackground(const sf::Color color)
{
	background = color;
	need_update = true;
}

sf::Color awt::Component::getBackground() const
{
	return background;
}

void awt::Component::setBorderColor(const sf::Color color)
{
	borderColor = color;
	need_update = true;
}

sf::Color awt::Component::getBorderColor() const
{
	return borderColor;
}

void awt::Component::setBorderThickness(const unsigned int thickness)
{
	borderThickness = thickness;
	need_update = true;
}

unsigned int awt::Component::getBorderThickness() const
{
	return borderThickness;
}

awt::Rectangle2D<int> awt::Component::getBounds() const
{
	return Rectangle2D<int>(getLocation(), getSize());
}

std::string awt::Component::toString() const
{
	return std::string("<awt::Component> [" + std::to_string(hashCode()) + "]");
}
