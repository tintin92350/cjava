#pragma once

#include "../lang/Object.h"

#include "Point.h"
#include "Dimension.h"
#include "Font.h"
#include "Rectangle2D.h"

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

namespace awt
{
	class Container;

	/**
	 * A component is an object having a graphical representation that can be displayed on the screen and that can interact with the user. Examples of components are the buttons, checkboxes, and scrollbars of a typical graphical user interface. 
	 */
	class Component : public lang::Object
	{
		protected:

			Component();
			~Component();

		public:

			/**
			 * Gets the location of this component in the form 
			 * of a point specifying the component's top-left corner.
			 */
			Point<int> getLocation() const;

			/**
			 * Stores the x,y origin of this component into 
			 * "return value" rv and return rv.
			 */
			Point<int> getLocation(Point<int> & rv);

			/**
			 * Moves this component to a new location. The top-left 
			 * corner of the new location is specified by the x and y 
			 * parameters in the coordinate space of this component's parent. 
			 */
			void setLocation(const int x, const int y);

			/**
			 * Moves this component to a new location. The top-left 
			 * corner of the new location is specified by point p. 
			 * Point p is given in the parent's coordinate space.
			 */
			void setLocation(const Point<int> p);

			/**
			 * Gets the maximum size of this component.
			 */
			Dimension<int> getMaximumSize() const;

			/**
			 * Sets the maximum size of this component.
			 */
			void setMaximumSize(const Dimension<int> maximumSize);

			/**
			 * Gets the minimum size of this component.
			 */
			Dimension<int> getMinimumSize() const;

			/**
			 * Sets the minimum size of this component.
			 */
			void setMinimumSize(const Dimension<int> minimumSize);

			/**
			 * Gets the preffered size of this component.
			 */
			Dimension<int> getPrefferedSize() const;

			/**
			 * Sets the preffered size of this component.
			 */
			void setPrefferedSize(const Dimension<int> prefferedSize);

			/**
			 * Gets the size of this component.
			 */
			Dimension<int> getSize() const;

			/**
			 * Sets the size of this component.
			 */
			void setSize(const Dimension<int> size);
			void setSize(const int w, const int h);

			/**
			 * Update this component
			 */
			virtual void update();

			/**
			 * Force update this component
			 */
			virtual void updateForced();

			/**
			 * Paints this component.
			 */
			virtual void paint(sf::RenderTarget & target, sf::RenderStates states);

			/**
			 * Handle event
			 */
			virtual void handleEventFromUser(const sf::Event& e);

			/**
			 * Set parent container
			 */
			void setParent(Container* p);

			/**
			 * Gets the parent of this component.
			 */
			Container* getParent();

			/**
			 * Returns the position of the mouse pointer in this Component's coordinate space 
			 * if the Component is directly under the mouse pointer, otherwise returns null.
			 */
			virtual Point<int> getMousePosition();

			/**
			 * Sets the font of this component.
			 */
			void setFont(Font* font);

			/**
			 * Gets the font of this component.
			 */
			Font * getFont();

			/**
			 * Sets the foreground color of this component.
			 */
			void setForeground(const sf::Color color);

			/**
			 * Gets the foreground color of this component.
			 */
			sf::Color getForeground() const;

			/**
			 * Sets the background color of this component.
			 */
			void setBackground(const sf::Color color);

			/**
			 * Gets the background color of this component.
			 */
			sf::Color getBackground() const;

			/**
			 * Sets the border color of this component.
			 */
			void setBorderColor(const sf::Color color);

			/**
			 * Gets the border color of this component.
			 */
			sf::Color getBorderColor() const;

			/**
			 * Gets the border thickness of this component.
			 */
			void setBorderThickness(const unsigned int thickness);

			/**
			 * Gets the border thickness of this component.
			 */
			unsigned int getBorderThickness() const;

			/**
			 * Gets the bounds of this component in the form of a Rectangle object.
			 */
			Rectangle2D<int> getBounds() const;

			/**
			 * Returns a string representation of the object.
			 */
			virtual std::string toString() const;

		private:

			/**
			 * Colors used by this component
			 */
			sf::Color foreground;
			sf::Color background;
			sf::Color borderColor;
			unsigned int borderThickness;

			/**
			 * Font used by this component
			 */
			Font* font;

			/**
			 * Parent container
			 */
			awt::Container* parent;

			/**
			 * Location of the component
			 */
			Point<int> location;

			/**
			 * Sizes of the component
			 */
			Dimension<int> size;
			Dimension<int> prefferedSize;
			Dimension<int> maximumSize;
			Dimension<int> minimumSize;

		protected:

			/**
			 * Component need to be updated graphicaly
			 */
			bool need_update;
	};
}