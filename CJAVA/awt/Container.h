#pragma once

#include <vector>

#include "../lang/Object.h"

#include "Point.h"
#include "Dimension.h"
#include "Component.h"
#include "LayoutManager.h"
#include "Insets.h"

namespace awt
{
	/**
	 * Component array
	 */
	typedef std::vector<awt::Component*>	ComponentArray;
	typedef std::vector<lang::Object*>		ConstraintArray;

	/**
	 * A component is an object having a graphical representation that can be displayed on the screen and that can interact with the user. Examples of components are the buttons, checkboxes, and scrollbars of a typical graphical user interface.
	 */
	class Container : public awt::Component
	{
		public:

			typedef std::shared_ptr<Container> PTR;

			/**
			 * Constructs a new Container.
			 */
			Container();
			~Container();

			/**
			 * Appends the specified component to the end of this container.
			 */
			void add(awt::Component* component);
			void add(awt::Component* component, lang::Object * constraint);

			/**
			 * Gets the nth component in this container.
			 */
			awt::Component* getComponent(const int n);

			/**
			 * Gets the number of components in this panel.
			 */
			unsigned int getComponentCount() const;

			/**
			 * Gets all the components in this container.
			 */
			ComponentArray getComponents();

			/**
			 * Gets the layout manager for this container.
			 */
			awt::ILayoutManager * getLayout();

			/**
			 * Gets the minimum size of this component.
			 */
			virtual Dimension<int> getMinimumSize();

			/**
			 * Gets the preffered size of this component.
			 */
			virtual Dimension<int> getPrefferedSize();

			/**
			 * Sets the layout manager for this container.
			 */
			void setLayout(awt::ILayoutManager * mgr);

			/**
			 * Sets the insets of this container,
			 * which indicate the size of the container's border.
			 */
			void setInsets(const Insets insets);

			/**
			 * Determines the insets of this container,
			 * which indicate the size of the container's border.
			 */
			Insets getInsets() const;

			/**
			 * Returns a string representation of the object.
			 */
			virtual std::string toString() const;

		protected:

			ComponentArray  components;
			ConstraintArray constraints;
			awt::ILayoutManager* layoutManager;

			/**
			 * Insets of the layout
			 */
			Insets insets;
	};
}