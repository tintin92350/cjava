#pragma once

// SFML
#include <SFML/Graphics/Font.hpp>

#include "../lang/Object.h"
#include "Rectangle2D.h"

namespace awt
{
	class Font : public lang::Object, public sf::Font
	{
		public:
		
			enum TYPE {
				PLAIN,
				BOLD,
				LIGHT
			};

			/**
			 * Default constructor
			 */
			Font(std::string name, TYPE type, int cs);

			/**
			 * Returns the point size of this Font, rounded to an integer.
			 */
			int getSize() const;

			/**
			 * Set font point size
			 */
			void setSize(const int cs);

			/**
			 * Returns the logical bounds of the specified array of characters
			 */
			awt::Rectangle2D<int> getStringBounds(const std::string str);

		private:

			unsigned int cs;

	};
}