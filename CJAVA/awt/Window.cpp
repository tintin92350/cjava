#include "Window.h"

#include <iostream>
#include <Windows.h>

using namespace awt;

awt::Window::Window() :
	caption(this)
{
	need_update = true;
	this->init();
}

Window::Window(const char* title) :
	caption(this)
{
	need_update = true;
	this->create(title);
	this->init();
}

void Window::init()
{
	defaultView = windowHandle.getDefaultView();
	backup = windowHandle.getDefaultView();

	close_state = false;

	isMaximized = false;
}

void Window::applyTheme(IWindowTheme* theme)
{
	this->theme = theme;

	if (titlebar_background_a.getVertexCount() > 3 && this->theme->WINDOW_CAPTION().size() >= 2)
	{
		titlebar_background_a[0].color = this->theme->WINDOW_CAPTION()[0];
		titlebar_background_a[1].color = this->theme->WINDOW_CAPTION()[0];
		titlebar_background_a[2].color = this->theme->WINDOW_CAPTION()[1];
		titlebar_background_a[3].color = this->theme->WINDOW_CAPTION()[1];
	}

	closeButton.setFillColor(this->theme->WINDOW_CLOSE_BUTTON());
	minimizeButton.setFillColor(this->theme->WINDOW_MINIMIZE_BUTTON());
	maximizeButton.setFillColor(this->theme->WINDOW_MAXIMIZE_BUTTON());
	titlebar_background.setOutlineColor(this->theme->WINDOW_CAPTION_BORDER_COLOR());
	titlebar_background.setOutlineThickness(static_cast<float>(this->theme->WINDOW_CAPTION_BORDER_THICKNESS()));
	title.setFillColor(this->theme->WINDOW_CAPTION_TITLE_COLOR());
}

void Window::create(const char* title)
{
	sf::ContextSettings settings(24);
	settings.antialiasingLevel = 8;

	sf::VideoMode modeExt(800, 632);

	windowHandle.create(modeExt, title, sf::Style::None, settings);

	float titlebar_height = 32.0f;

	titlebar_background.setSize(sf::Vector2f(800, 32));

	titlebar_background_a = sf::VertexArray(sf::Quads);
	titlebar_background_a.append(sf::Vertex(sf::Vector2f(0.0f, 0.0f)));
	titlebar_background_a.append(sf::Vertex(sf::Vector2f(800.0f, 0.0f)));
	titlebar_background_a.append(sf::Vertex(sf::Vector2f(800.0f, 32.f)));
	titlebar_background_a.append(sf::Vertex(sf::Vector2f(0.0f, 32.f)));

	closeButton.setPosition(12.0f, 10.0f);
	closeButton.bindFunction([this] {this->close_state = true; });

	minimizeButton.setPosition(32.f, 10.0f);
	minimizeButton.bindFunction(std::bind(&Window::minimize, this));

	maximizeButton.setPosition(52.0f, 10.0f);
	maximizeButton.bindFunction(std::bind(&Window::maximize, this));

	setFont(new Font("fonts/LucidaGrande.ttf", Font::TYPE::BOLD, 16));

	this->title = sf::Text(title, *getFont(), getFont()->getSize());
	sf::FloatRect rect = this->title.getLocalBounds();
	this->title.setOrigin(roundf(rect.left + rect.width / 2.0f), roundf(rect.top + rect.height / 2.0f));
	this->title.setPosition((float)modeExt.width / 2.0f, 16.0f);

	HRGN RoundedWindowArea = CreateRoundRectRgn(0, 0, modeExt.width, modeExt.height, 5, 5);
		SetWindowRgn(windowHandle.getSystemHandle(), RoundedWindowArea, TRUE);

	applyTheme(new DefaultWindowThemeDark());

	lastSize = awt::Dimension<int>(modeExt.width, modeExt.height);

	lastPosition = awt::Point<int>(windowHandle.getPosition().x, windowHandle.getPosition().y);

	caption.setZone(sf::IntRect(0, 0, modeExt.width, 32));

	setSize(awt::Dimension<int>(modeExt.width, modeExt.height));
	setLocation(awt::Point<int>(windowHandle.getPosition().x, windowHandle.getPosition().y));
}

void awt::Window::run()
{
	getLayout()->layoutContainer(this);

	contentPane->updateForced();

	while (isOpen())
	{
		handleEvents();
		clear();
		display();
	}
}

bool Window::handleEvents()
{
	sf::Event event;

	while (windowHandle.pollEvent(event))
	{
		contentPane->handleEventFromUser(event);
		processEvents(event);
	}

	if (close_state) {
		windowHandle.close();
		return true;
	}

	update();

	return true;
}

void Window::clear(sf::Color clr)
{
	windowHandle.clear(this->theme->WINDOW_BACKGROUND());
}

void Window::draw(const sf::Drawable& drawable, const sf::RenderStates& states)
{
	sf::RenderStates nstates = states;
	nstates.transform.translate(sf::Vector2f(0.0f, 32.0f));

	windowHandle.draw(drawable, nstates);
}

void Window::display()
{
	sf::RenderStates states;
	states.transform.translate(0.0f, 32.0f);

	contentPane->update();
	contentPane->paint(windowHandle, states);
	
	windowHandle.draw(titlebar_background_a);
	windowHandle.draw(title);
	windowHandle.draw(closeButton);
	windowHandle.draw(minimizeButton);
	windowHandle.draw(maximizeButton);

	windowHandle.display();
}

void Window::minimize()
{
	HWND hWnd = windowHandle.getSystemHandle(); //What do I add here to minimize the window??
	ShowWindow(hWnd, SW_MINIMIZE);
}

void Window::maximize()
{
	isMaximized = !isMaximized;

	if (isMaximized)
	{
		lastSize = getSize();
		this->setSize(awt::Dimension<int>(sf::VideoMode::getDesktopMode().width, sf::VideoMode::getDesktopMode().height));
		this->setLocation(awt::Point<int>(0, 0));
		windowHandle.setPosition(sf::Vector2i(getLocation().getX(), getLocation().getY()));
		windowHandle.setSize(sf::Vector2u(getSize().getWidth(), getSize().getHeight() + 32));

		defaultView = sf::View(sf::Vector2f(sf::VideoMode::getDesktopMode().width / 2.0f, sf::VideoMode::getDesktopMode().height / 2.0f), sf::Vector2f(static_cast<float>(sf::VideoMode::getDesktopMode().width), static_cast<float>(sf::VideoMode::getDesktopMode().height)));
	}
	else
	{
		this->setSize(lastSize);
		this->setLocation(lastPosition);
		windowHandle.setPosition(sf::Vector2i(getLocation().getX(), getLocation().getY()));
		windowHandle.setSize(sf::Vector2u(getSize().getWidth(), getSize().getHeight()+32));

		defaultView = backup;
	}

	need_update = true;
	update();
}

void awt::Window::close()
{
	windowHandle.close();
}

bool Window::isCursorHoverTitleBar() const
{
	sf::Vector2f cursor(sf::Mouse::getPosition(windowHandle));

	return cursor.y <= 32.0f;
}

sf::View Window::getDefaultView() const
{
	return defaultView;
}

sf::Vector2u Window::getContextSize() const
{
	return windowHandle.getSize() + sf::Vector2u(0, -32);
}

void awt::Window::update()
{
	if (need_update)
	{
		titlebar_background_a[0].position = sf::Vector2f(0.0f, 0.0f);
		titlebar_background_a[1].position = sf::Vector2f(static_cast<float>(this->getSize().getWidth()), 0.0f);
		titlebar_background_a[2].position = sf::Vector2f(static_cast<float>(this->getSize().getWidth()), 32.f);
		titlebar_background_a[3].position = sf::Vector2f(0.0f, 32.f);
		titlebar_background.setSize(sf::Vector2f(static_cast<float>(this->getSize().getWidth()), 32.0f));

		this->title.setPosition(static_cast<float>(this->getSize().getWidth()) / 2.0f, 16.0f);

		getLayout()->layoutContainer(this);

		windowHandle.setPosition(sf::Vector2i(getLocation().getX(), getLocation().getY()));
		windowHandle.setSize(sf::Vector2u(getSize().getWidth(), getSize().getHeight() + 32));

		sf::FloatRect visibleArea(0.0f, 0.0f, static_cast<float>(getSize().getWidth()), static_cast<float>(getSize().getHeight()+32));
		windowHandle.setView(sf::View(visibleArea));

		caption.setZone(sf::IntRect(0, 0, getSize().getWidth(), 32));

		HRGN RoundedWindowArea = CreateRoundRectRgn(0, 0, getSize().getWidth(), getSize().getHeight()+32, 5, 5);
		SetWindowRgn(windowHandle.getSystemHandle(), RoundedWindowArea, TRUE);


		need_update = false;
	}
}

bool awt::Window::isOpen() const
{
	return windowHandle.isOpen();
}

std::string awt::Window::toString() const
{
	return std::string("<awt::Window> [" + std::to_string(hashCode()) + "]");
}

Point<int> awt::Window::getMousePosition()
{
	sf::Vector2i mouse(sf::Mouse::getPosition(windowHandle));
	return Point<int>(mouse.x, mouse.y - 32);
}

void awt::Window::setContentPane(Container* container)
{
	this->contentPane = std::shared_ptr<Container>(std::move(container));
	this->contentPane->setParent(this);
	this->contentPane->setSize(this->getSize());
}

void Window::processEvents(sf::Event & event)
{
	closeButton.handleEvents(event);
	minimizeButton.handleEvents(event);
	maximizeButton.handleEvents(event);

	if (closeButton.getCurrentState()		== WindowButton::State::NORMAL	&&
		minimizeButton.getCurrentState()	== WindowButton::State::NORMAL	&&
		maximizeButton.getCurrentState()	== WindowButton::State::NORMAL	&&
		!isMaximized)
	{
		if (caption.drag(event))
			need_update = true;
	}
}
