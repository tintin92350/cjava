#pragma once

#include "../lang/Object.h"

#include "Point.h"
#include "Dimension.h"

#include <SFML/Graphics/Rect.hpp>

namespace awt
{
	/**
	 * The Rectangle2D class describes a rectangle 
	 * defined by a location (x,y) and dimension (w x h). 
	 */
	template < typename PrimType >
	class Rectangle2D : public lang::Object, public sf::Rect<PrimType>
	{
		public:

			Rectangle2D() {
				this->top = 0;
				this->left = 0;
				this->width = 0;
				this->height = 0;
			}

			Rectangle2D(PrimType x, PrimType y, PrimType width, PrimType height) {
				this->left = x;
				this->top = y;
				this->width = width;
				this->height = height;
			}

			Rectangle2D(Point<PrimType> p, Dimension<PrimType> d) {
				this->left = p.getX();
				this->top = p.getY();
				this->width = d.getWidth();
				this->height = d.getHeight();
			}
	};
}