#include "Insets.h"

awt::Insets::Insets()
{
	this->top = 0;
	this->left = 0;
	this->bottom = 0;
	this->right = 0;
}

awt::Insets::Insets(const int top, const int left, const int bottom, const int right)
{
	this->top = top;
	this->left = left;
	this->bottom = bottom;
	this->right = right;
}
