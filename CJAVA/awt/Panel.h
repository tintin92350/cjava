#pragma once

// AWT
#include "Container.h"
#include "LayoutManager.h"
#include "Window.h"

namespace awt
{
	/**
	 * Panel is the simplest container class. 
	 * A panel provides space in which an application can attach 
	 * any other component, including other panels. 
	 */
	class Panel : public awt::Container
	{
		public:

			Panel();
			Panel(ILayoutManager * layout);
			
			/**
			 * Sets window parent for handling and painting
			 */
			void setWindowParent(Window * win);

			/**
			 * Returns the window parent for handling and painting
			 */
			Window * getWindowParent();

			/**
			 * Update this component
			 */
			virtual void update();

			/**
			 * Update this component
			 */
			virtual void updateForced();

			/**
			 * Paints this component.
			 */
			virtual void paint(sf::RenderTarget& target, sf::RenderStates states);

			/**
			 * Handle event
			 */
			virtual void handleEventFromUser(const sf::Event& e);

			/**
			 * Returns the position of the mouse pointer in this Component's coordinate space
			 * if the Component is directly under the mouse pointer, otherwise returns null.
			 */
			virtual Point<int> getMousePosition();

			typedef std::shared_ptr<awt::Panel> PTR;

		private:

			Window * window;
	};
}