#pragma once

#include <vector>

#include "../lang/Object.h"

#include "Point.h"
#include "Dimension.h"
#include "Component.h"
#include "Insets.h"

#include "LayoutManager.h"

namespace awt
{
	/**
	 * The FreeWayLayout handle nothing.
	 * Its allow a way of positionning totally free
	 */
	class FreeWayLayout : public lang::Object, public ILayoutManager
	{
		public:

			/**
			 * Creates a free way layout
			 */
			FreeWayLayout();

			/**
			 * Lays out the specified container.
			 */
			virtual void layoutContainer(awt::Container* container);

			/**
			 * Calculates the minimum size dimensions for the 
			 * specified container, given the components it contains.
			 */
			virtual Dimension<int> minimumLayoutSize(Container* parent);

			/**
			 * Calculates the preferred size dimensions for the 
			 * specified container, given the components it contains.
			 */
			virtual Dimension<int> preferredLayoutSize(Container *parent);
	};
}