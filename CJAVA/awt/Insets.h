#pragma once

#include "../lang/Object.h"

namespace awt
{
	/**
	 * An Insets object is a representation of the borders of a container. 
	 * It specifies the space that a container must leave at each of its edges. 
	 * The space can be a border, a blank space, or a title.
	 */
	class Insets : public lang::Object
	{
		public:

			Insets();
			Insets(const int top, const int left, const int bottom, const int right);

			/**
			 * Creates and returns a copy of this object.
			 */
			virtual Object::PTR clone() {
				return std::make_shared<Insets>(top, left, bottom, right);
			}

			int top;
			int left;
			int bottom;
			int right;
	};
}