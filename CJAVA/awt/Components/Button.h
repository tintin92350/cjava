#pragma once

// STD
#include <string>

// CJava
#include "../Component.h"
#include "../../util/Event/IActionListener.h"

// SFML
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/RoundedRectangleShape.hpp>

namespace awt
{
	class Button : public awt::Component, public std::enable_shared_from_this<Button>
	{
		public:

			Button();
			Button(const std::string text);
			~Button();


			// PTR
			typedef std::shared_ptr<Button> PTR;

			/**
			 * Create a shared pointer from pointer
			 */
			static Button::PTR create(Button* l) {
				return Button::PTR(l);
			}

			// Met la source du text de bouton
			void setText(const std::string text);
			// Retourne le text du bouton
			std::string getText() const;

			/**
			 * Returns the action command for this button.
			 */
			std::string getActionCommand() const;

			/**
			 * Sets the action command for this button.
			 */
			void setActionCommand(const std::string cmd);

			/**
			 * Adds an ActionListener to the button.
			 */
			void addActionListener(IActionListener* l);

			virtual void handleEventFromUser(const sf::Event& e);
			virtual void update();
			virtual void paint(sf::RenderTarget& target, sf::RenderStates states);

			/**
			 * Returns a string representation of the object.
			 */
			virtual std::string toString() const;

			std::shared_ptr<Button> share() {
				return shared_from_this();
			}


		private:

			/**
			 * Action command
			 */
			std::string actionCommand;

			/**
			 * Action listeners attached to this component
			 */
			std::vector<IActionListener*> actionListeners;

			// Text de l'etiquette
			std::string text_source;

			// Orientation du text
			int orientation;

			// Graphique
			sf::Text text;
			sf::RoundedRectangleShape shapeBackground;

			bool pressed;
	};
}