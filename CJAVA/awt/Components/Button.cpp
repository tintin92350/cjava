#include "Button.h"

#include "../../lang/Object.h"
#include "../../util/ActionEvent.h"

#include "../Font.h"

#include "../Container.h"

using namespace awt;

void Button::update()
{
	if (need_update)
	{
		if (getFont()) {
			text.setFont(*getFont());
			text.setCharacterSize(getFont()->getSize());
		}
		text.setString(text_source);
		text.setOrigin(roundf(text.getLocalBounds().left + text.getLocalBounds().width / 2.0f), roundf(text.getLocalBounds().top + text.getLocalBounds().height / 2.0f));
		text.setFillColor(getForeground());
		text.setPosition(getLocation().getX() + getSize().getWidth() / 2.0f, getLocation().getY() + getSize().getHeight() / 2.0f);

		shapeBackground.setCornerPointCount(100);
		shapeBackground.setCornersRadius(3);
		shapeBackground.setPosition(static_cast<float>(getLocation().getX()), static_cast<float>(getLocation().getY()));
		shapeBackground.setSize(sf::Vector2f(static_cast<float>(getSize().getWidth()), static_cast<float>(getSize().getHeight())));
		shapeBackground.setFillColor(getBackground());
		shapeBackground.setOutlineColor(sf::Color(230, 230, 230));
		shapeBackground.setOutlineThickness(1);

		need_update = false;
	}
}

Button::Button() :
	pressed(false)
{
	setText("Bouton");
	setBackground(sf::Color::White);
	setForeground(sf::Color(60, 60, 60));
	setFont(new Font("fonts/LucidaGrande.ttf", Font::TYPE::BOLD, 14));
	setSize(120, 18);

	need_update = true;
	update();
}

Button::Button(const std::string text) :
	pressed(false)
{
	setText(text);
	setBackground(sf::Color::White);
	setForeground(sf::Color(60, 60, 60));
	setFont(new Font("fonts/LucidaGrande.ttf", Font::TYPE::BOLD, 14));
	setSize(120, 18);

	need_update = true;
	update();
}

Button::~Button()
{
}

void Button::setText(const std::string text)
{
	text_source = text;
	need_update = true;
}

std::string Button::getText() const
{
	return text_source;
}

std::string awt::Button::getActionCommand() const
{
	return actionCommand;
}

void awt::Button::setActionCommand(const std::string cmd)
{
	actionCommand = cmd;
}

void Button::handleEventFromUser(const sf::Event& e)
{
	if (e.type == sf::Event::MouseMoved && !pressed)
	{
		sf::Vector2i cursor(getParent()->getMousePosition().getX(), getParent()->getMousePosition().getY());
		
		if (getBounds().contains(cursor))
		{
			setBackground(sf::Color(245, 245, 245));
		}
		else
		{
			setBackground(sf::Color::White);
		}
	}
	if (e.type == sf::Event::MouseButtonPressed)
	{
		sf::Vector2i cursor(getParent()->getMousePosition().getX(), getParent()->getMousePosition().getY());

		pressed = this->getBounds().contains(cursor);

		if (pressed)
		{
			setBackground(sf::Color(150, 150, 150));
			setForeground(sf::Color::White);
		}

		need_update = true;
	}
	else if (e.type == sf::Event::MouseButtonReleased)
	{
		sf::Vector2i cursor(getParent()->getMousePosition().getX(), getParent()->getMousePosition().getY());

		if (pressed && this->getBounds().contains(cursor))
		{
			for (auto& actionListener : actionListeners)
				actionListener->actionPerformed(util::ActionEvent(share(), actionCommand));
		}
		else
			pressed = false;

		setBackground(sf::Color::White);
		setForeground(sf::Color(60, 60, 60));

		need_update = true;
	}
}

void awt::Button::paint(sf::RenderTarget& target, sf::RenderStates states)
{
	target.draw(shapeBackground, states);
	target.draw(text, states);
}

std::string awt::Button::toString() const
{
	return std::string("<awt::Component::Button> [" + std::to_string(hashCode()) + "]");
}

void awt::Button::addActionListener(IActionListener* l)
{
	actionListeners.push_back(l);
}