#include "TextArea.h"

#include "../Container.h"

awt::TextArea::TextArea() :
	cursorPosition(0),
	active(false),
	clicked(false)
{
	setBackground(sf::Color::White);
	setForeground(sf::Color(20, 20, 20));
	setBorderColor(sf::Color(220, 220, 220));
	setBorderThickness(1);
	setFont(new Font("fonts/LucidaGrande.ttf", Font::TYPE::BOLD, 14));
	setSize(150, 18);

	need_update = true;
	update();
}

awt::TextArea::TextArea(const std::string string) :
	cursorPosition(0),
	active(false),
	clicked(false)
{
	setPlaceholder(string);
	setBackground(sf::Color::White);
	setForeground(sf::Color(20, 20, 20));
	setBorderColor(sf::Color(220, 220, 220));
	setBorderThickness(1);
	setFont(new Font("fonts/LucidaGrande.ttf", Font::TYPE::BOLD, 14));
	setSize(150, 18);

	need_update = true;
	update();
}

std::string awt::TextArea::getString() const
{
	return string;
}

void awt::TextArea::setString(const std::string str)
{
	string = str;
	cursorPosition = string.getSize();

	need_update = true;	
}

void awt::TextArea::setSelected(const bool s)
{
	active = s;
}

void awt::TextArea::setPlaceholder(const std::string pholder)
{
	placeholder.setString(pholder);
}

void awt::TextArea::append(const std::string str, bool new_line)
{
	setString(getString() + (new_line && !getString().empty() ? "\n" : "") + str);
}

void awt::TextArea::handleEventFromUser(const sf::Event& e)
{
	if (e.type == sf::Event::MouseButtonPressed)
	{
		sf::Vector2i cursor(getParent()->getMousePosition().getX(), getParent()->getMousePosition().getY());

		if (getBounds().contains(cursor))
		{
			clicked = true;
			setBackground(sf::Color::White);
			setForeground(sf::Color(20, 20, 20));
		}
		else
		{
			active = false;
			setBackground(sf::Color::White);
			setForeground(sf::Color(20, 20, 20));
			setBorderColor(sf::Color(220, 220, 220));
			setBorderThickness(1);
		}
	}
	else if (e.type == sf::Event::MouseButtonReleased && clicked)
	{
		sf::Vector2i cursor(getParent()->getMousePosition().getX(), getParent()->getMousePosition().getY());

		if (clicked && getBounds().contains(cursor))
		{
			active = true;
			setBackground(sf::Color::White);
			setForeground(sf::Color(20, 20, 20));
			setBorderColor(sf::Color(10, 140, 245, 200));
			setBorderThickness(3);
		}
		else if(active)
		{
			clicked = false;
			setBackground(sf::Color::White);
			setForeground(sf::Color(20, 20, 20));
			setBorderColor(sf::Color(220, 220, 220));
			setBorderThickness(1);
		}
	}
	else if (e.type == sf::Event::KeyPressed && active)
	{
		if (e.key.code == sf::Keyboard::Left) {
			this->cursorPosition--;
			if (this->cursorPosition < 0)
				this->cursorPosition = 0;
		}
		else if (e.key.code == sf::Keyboard::Right) {
			this->cursorPosition++;
			if (this->cursorPosition > (int)this->string.getSize())
				this->cursorPosition = (int)this->string.getSize();
		}
		else if (e.key.code == sf::Keyboard::Return)
		{
			this->string.insert(this->cursorPosition++, '\n');
		}
		else if (e.key.code == sf::Keyboard::BackSpace)
		{
			if (!this->string.isEmpty())
				this->string.erase(this->cursorPosition - 1, 1);
			this->cursorPosition--;
			if (this->cursorPosition < 0)
				this->cursorPosition = 0;

			need_update = true;
		}

		cursorAlpha = 255;

		for (auto& keyListener : keyListeners)
			keyListener->KeyPressed(util::KeyEvent(share(), e.key.code));
	}
	else if (e.type == sf::Event::TextEntered && active)
	{
		int unicode = e.text.unicode;

		if (unicode >= 32 && unicode <= 126) {
			this->string.insert(this->cursorPosition++, static_cast<char>(e.text.unicode));

			for (auto& keyListener : keyListeners)
				keyListener->KeyTyped(util::KeyEvent(share(), e.text.unicode));
		}

		cursorAlpha = 255;

		need_update = true;
	}
}

void awt::TextArea::update()
{
	if (need_update)
	{
		if (getFont()) {
			placeholder.setFont(*getFont());
			placeholder.setCharacterSize(getFont()->getSize());

			text.setFont(*getFont());
			text.setCharacterSize(getFont()->getSize());
		}

		placeholder.setFillColor(getForeground());
		placeholder.setOrigin(roundf(placeholder.getLocalBounds().left), roundf(placeholder.getLocalBounds().top));
		placeholder.setPosition(static_cast<float>(getLocation().getX()) + 4.0f, static_cast<float>(getLocation().getY()) + 4.0f);

		text.setString(string);
		text.setOrigin(roundf(text.getLocalBounds().left), roundf(text.getLocalBounds().top));
		text.setFillColor(getForeground());
		text.setPosition(static_cast<float>(getLocation().getX()) + 4.0f, static_cast<float>(getLocation().getY()) + 4.0f);

		shapeBackground.setPosition(static_cast<float>(getLocation().getX()), static_cast<float>(getLocation().getY()));
		shapeBackground.setSize(sf::Vector2f(static_cast<float>(getSize().getWidth()), static_cast<float>(getSize().getHeight())));
		shapeBackground.setFillColor(getBackground());
		shapeBackground.setOutlineColor(getBorderColor());
		shapeBackground.setOutlineThickness(static_cast<float>(getBorderThickness()));

		sf::FloatRect zone(getLocation().getX(), getLocation().getY() + 32.0f, getSize().getWidth() - 4.0f, getSize().getHeight());
		sf::FloatRect viewport(zone.left / 720.0f, zone.top / (88.0f+32.0f), zone.width / 720.0f, zone.height / (88.0f+32.0f));

		view = sf::View(zone);
		view.setViewport(viewport);

		need_update = false;
	}
}

void awt::TextArea::paint(sf::RenderTarget& target, sf::RenderStates states)
{
	target.draw(shapeBackground, states);

	sf::View oldView = target.getView();

	target.setView(view);

	if (!string.isEmpty())
		target.draw(text, states);

	if (active)
	{
		sf::RectangleShape cursorPosition(sf::Vector2f(1.0f, getFont()->getSize()));
		cursorAlpha += 0.1f;
		cursorPosition.setFillColor(sf::Color(0, 0, 0, cursorAlpha));

		if (string.isEmpty())
			cursorPosition.setPosition(roundf(getLocation().getX() + 4.0f), roundf(getLocation().getY() + 4.0f));
		else
			cursorPosition.setPosition(roundf(text.findCharacterPos(this->cursorPosition).x), roundf(text.findCharacterPos(this->cursorPosition).y + 2));

		target.draw(cursorPosition, states);
	}
	else if (string.isEmpty())
		target.draw(placeholder, states);
	target.setView(oldView);
}

std::string awt::TextArea::toString() const
{
	return std::string("<awt::Component::TextArea> [" + std::to_string(hashCode()) + "]");
}
