#include "Label.h"

#include "../Container.h"

awt::Label::Label()
{
	setFont(new Font("fonts/LucidaGrande.ttf", Font::TYPE::PLAIN, 11));
	setText("");
	setOrientation(ORIENTATION::LEFT);
	setForeground(sf::Color::Black);
	setBorderColor(sf::Color::White);
	setBorderThickness(0);
	setOrientationVertical(ORIENTATION_VERTICAL::TOP);
	setSize(0, 16);

	update();
}

awt::Label::Label(const std::string text)
{
	setFont(new Font("fonts/LucidaGrande.ttf", Font::TYPE::PLAIN, 11));
	setText(text);
	setOrientation(ORIENTATION::LEFT);
	setForeground(sf::Color::Black);
	setBorderColor(sf::Color::White);
	setBorderThickness(0);
	setOrientationVertical(ORIENTATION_VERTICAL::TOP);
	setSize(0, 16);
	update();
}

awt::Label::Label(const std::string text, const int orientation)
{
	setFont(new Font("fonts/LucidaGrande.ttf", Font::TYPE::PLAIN, 11));
	setText(text);
	setOrientation(orientation);
	setForeground(sf::Color::Black);
	setFont(new Font("fonts/LucidaGrande.ttf", Font::TYPE::PLAIN, 11));
	setBorderColor(sf::Color::White);
	setBorderThickness(0);
	setOrientationVertical(ORIENTATION_VERTICAL::TOP);
	setSize(0, 16);
	update();
}

awt::Label::~Label()
{
}

void awt::Label::setText(const std::string text)
{
	text_source = text;

	need_update = true;
}

std::string awt::Label::getText() const
{
	return text_source;
}

void awt::Label::setOrientation(const int orientation)
{
	this->orientation = orientation;

	need_update = true;
}

int awt::Label::getOrientation() const
{
	return orientation;
}

void awt::Label::setOrientationVertical(const int orientation)
{
	orientation_vertical = orientation;
}

int awt::Label::getOrientationVertical() const
{
	return orientation_vertical;
}

void awt::Label::handleEventFromUser(const sf::Event& e)
{
}

void awt::Label::update()
{
	if (need_update)
	{
		
		text.setCharacterSize(getFont()->getSize());
		text.setFont(*getFont());
		text.setString(text_source);

		sf::FloatRect r = text.getLocalBounds();
		text.setOrigin(roundf(r.left), roundf(r.top));
		setSize(static_cast<int>(text.getLocalBounds().width), static_cast<int>(text.getLocalBounds().height));
		
		text.setFillColor(getForeground());
		text.setOutlineThickness(static_cast<float>(getBorderThickness()));
		text.setOutlineColor(getBorderColor());
		
		text.setPosition(static_cast<float>(getLocation().getX()), static_cast<float>(getLocation().getY()));

		need_update = false;
	}
}

void awt::Label::paint(sf::RenderTarget& target, sf::RenderStates states)
{
	target.draw(text, states);
}

std::string awt::Label::toString() const
{
	return std::string("<awt::Component::Label> [" + std::to_string(hashCode()) + "]");
}
