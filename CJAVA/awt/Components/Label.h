#pragma once

// STD
#include <string>

// CJava
#include "../Component.h"
#include "../../util/Event/IActionListener.h"

// SFML
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/RoundedRectangleShape.hpp>

namespace awt
{
	class Label : public awt::Component, public std::enable_shared_from_this<Label>
	{
		public:

			Label();
			Label(const std::string text);
			Label(const std::string text, const int orientation);
			~Label();

			// PTR
			typedef std::shared_ptr<Label> PTR;

			/**
			 * Create a shared pointer from pointer
			 */
			static Label::PTR create(Label* l) {
				return Label::PTR(l);
			}

			enum ORIENTATION { LEFT, MIDDLE, RIGHT };
			enum ORIENTATION_VERTICAL { TOP, VMIDDLE, BOTTOM };

			// Met la source du text de bouton
			void setText(const std::string text);
			// Retourne le text du bouton
			std::string getText() const;

			// Met l'orientation du text
			void setOrientation(const int orientation);
			// Retourne l'orientation du text
			int getOrientation() const;
			// Met l'orientation du text
			void setOrientationVertical(const int orientation);
			// Retourne l'orientation du text
			int getOrientationVertical() const;

			virtual void handleEventFromUser(const sf::Event& e);
			virtual void update();
			virtual void paint(sf::RenderTarget& target, sf::RenderStates states);

			/**
			 * Returns a string representation of the object.
			 */
			virtual std::string toString() const;


			std::shared_ptr<Label> share() {
				return shared_from_this();
			}

		private:

			// Text de l'etiquette
			std::string text_source;

			// Orientation du text
			int orientation;
			int orientation_vertical;

			// Graphique
			sf::Text text;

			bool pressed;
	};
}