﻿#pragma once

// STD
#include <string>

// CJava
#include "../Component.h"
#include "../../util/Event/IActionListener.h"
#include "../../util/Event/IKeyListener.h"

// SFML
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/RoundedRectangleShape.hpp>

namespace awt
{
	class TextArea : public awt::Component, public std::enable_shared_from_this<TextArea>
	{
		public:

			// Constructeur par d�faut
			TextArea();
			TextArea(const std::string string);

			// PTR
			typedef std::shared_ptr<TextArea> PTR;

			/**
			 * Create a shared pointer from pointer
			 */
			static TextArea::PTR create(TextArea* l) {
				return TextArea::PTR(l);
			}

			// Retourne le text sous forme de chaine de caractere
			std::string getString() const;
			// Set string
			void setString(const std::string str);

			// Selectione ou non le champ
			void setSelected(const bool s = true);

			// Set placeholder
			void setPlaceholder(const std::string pholder);

			/**
			 * Append a text
			 */
			void append(const std::string str, bool new_line = false);

			/**
			 * Inherted methods from component
			 */
			virtual void handleEventFromUser(const sf::Event& e);
			virtual void update();
			virtual void paint(sf::RenderTarget& target, sf::RenderStates states);

			/**
			 * Returns a string representation of the object.
			 */
			virtual std::string toString() const;

			std::shared_ptr<TextArea> share() {
				return shared_from_this();
			}

		private:

			sf::Text			text;
			sf::Text			placeholder;
			float				cursorAlpha;
			sf::RectangleShape	shapeBackground;
			sf::String			string;
			int					cursorPosition;
			sf::View			view;

			bool				clicked;
			bool				active;

			std::vector<IKeyListener*> keyListeners;
	};
}