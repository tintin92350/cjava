﻿#pragma once

// STD
#include <string>

// CJava
#include "../Component.h"
#include "../../util/Event/IActionListener.h"
#include "../../util/Event/IKeyListener.h"

// SFML
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/RoundedRectangleShape.hpp>

namespace awt
{
	class TextField : public awt::Component, public std::enable_shared_from_this<TextField>
	{
		public:

			// Constructeur par d�faut
			TextField();
			TextField(const std::string string);
			TextField(const int width);

			// PTR
			typedef std::shared_ptr<TextField> PTR;

			/**
			 * Create a shared pointer from pointer
			 */
			static TextField::PTR create(TextField* l) {
				return TextField::PTR(l);
			}

			// Retourne le text sous forme de chaine de caractere
			std::string getString() const;
			// Set string
			void setString(const std::string str);

			// Selectione ou non le champ
			void setSelected(const bool s = true);

			// Set placeholder
			void setPlaceholder(const std::string pholder);

			/**
			 * Add a key listener
			 */
			void addKeyListener(IKeyListener* l);

			/**
			 * Inherted methods from component
			 */
			virtual void handleEventFromUser(const sf::Event& e);
			virtual void update();
			virtual void paint(sf::RenderTarget& target, sf::RenderStates states);

			/**
			 * Returns a string representation of the object.
			 */
			virtual std::string toString() const;

			std::shared_ptr<TextField> share() {
				return shared_from_this();
			}

		private:

			sf::Text			text;
			sf::Text			placeholder;
			sf::RectangleShape	shapeBackground;
			sf::String			string;
			int					cursorPosition;
			sf::View			view;
			int					view_move;
			float				diff;

			bool				clicked;
			bool				active;

			std::vector<IKeyListener*> keyListeners;
	};
}