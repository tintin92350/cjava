#include "Container.h"

#include "FlowLayout.h"

awt::Container::Container() :
	layoutManager(new FlowLayout()	)
{
}

awt::Container::~Container()
{
}

void awt::Container::add(awt::Component* component)
{
	component->setParent(this);
	components.push_back(component);
	constraints.push_back(nullptr);
}

void awt::Container::add(awt::Component* component, lang::Object* constraint)
{
	component->setParent(this);
	components.push_back(component);
	constraints.push_back(constraint);
}

awt::Component* awt::Container::getComponent(const int n)
{
	return components[n];
}

unsigned int awt::Container::getComponentCount() const
{
	return components.size();
}

awt::ComponentArray awt::Container::getComponents()
{
	return components;
}

awt::ILayoutManager* awt::Container::getLayout()
{
	return layoutManager;
}

awt::Dimension<int> awt::Container::getMinimumSize()
{
	return layoutManager->minimumLayoutSize(this);
}

awt::Dimension<int> awt::Container::getPrefferedSize()
{
	return layoutManager->preferredLayoutSize(this);
}

void awt::Container::setLayout(awt::ILayoutManager* mgr)
{
	layoutManager = mgr;
}

void awt::Container::setInsets(const Insets insets)
{
	this->insets = insets;
}

awt::Insets awt::Container::getInsets() const
{
	return insets;
}

std::string awt::Container::toString() const
{
	return std::string("<awt::Component::Container> [" + std::to_string(hashCode()) + "]");
}
