#pragma once

#include <SFML/Graphics/Color.hpp>

typedef std::vector<sf::Color> ColorArray;

__interface IWindowTheme
{
	public:

		virtual sf::Color WINDOW_BACKGROUND()		= 0;
		virtual ColorArray WINDOW_CAPTION()			= 0;
		virtual sf::Color WINDOW_CLOSE_BUTTON()		= 0;
		virtual sf::Color WINDOW_MINIMIZE_BUTTON()	= 0;
		virtual sf::Color WINDOW_MAXIMIZE_BUTTON()	= 0;
		virtual sf::Color WINDOW_CAPTION_BORDER_COLOR()			= 0;
		virtual unsigned int WINDOW_CAPTION_BORDER_THICKNESS()	= 0;
		virtual sf::Color WINDOW_CAPTION_TITLE_COLOR()			= 0;
};

class DefaultWindowThemeDark : public IWindowTheme
{
	public:

		virtual sf::Color WINDOW_BACKGROUND() {
			return sf::Color(30, 30, 30);
		}
		virtual ColorArray WINDOW_CAPTION() {
			return { sf::Color(35, 35, 35) };
		}
		virtual sf::Color WINDOW_CLOSE_BUTTON() {
			return sf::Color(203, 45, 62);
		}
		virtual sf::Color WINDOW_MINIMIZE_BUTTON() {
			return sf::Color(214, 149, 72);
		}
		virtual sf::Color WINDOW_MAXIMIZE_BUTTON() {
			return sf::Color(100, 185, 85);
		}
		virtual sf::Color WINDOW_CAPTION_BORDER_COLOR() {
			return sf::Color(25, 25, 25);
		}
		virtual unsigned int WINDOW_CAPTION_BORDER_THICKNESS() {
			return 2;
		}
		virtual sf::Color WINDOW_CAPTION_TITLE_COLOR() {
			return sf::Color::White;
		}
};

class DefaultWindowThemeLight : public IWindowTheme
{
	public:

		virtual sf::Color WINDOW_BACKGROUND() {
			return sf::Color(240, 240, 240);
		}
		virtual ColorArray WINDOW_CAPTION() {
			return { sf::Color(232, 230, 231), sf::Color(220, 216, 217) };
		}
		virtual sf::Color WINDOW_CLOSE_BUTTON() {
			return sf::Color(203, 45, 62);
		}
		virtual sf::Color WINDOW_MINIMIZE_BUTTON() {
			return sf::Color(214, 149, 72);
		}
		virtual sf::Color WINDOW_MAXIMIZE_BUTTON() {
			return sf::Color(100, 185, 85);
		}
		virtual sf::Color WINDOW_CAPTION_BORDER_COLOR() {
			return sf::Color(200, 200, 200);
		}
		virtual unsigned int WINDOW_CAPTION_BORDER_THICKNESS() {
			return 2;
		}
		virtual sf::Color WINDOW_CAPTION_TITLE_COLOR() {
			return sf::Color::Black;
		}
};
