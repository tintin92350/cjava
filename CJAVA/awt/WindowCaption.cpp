#include "WindowCaption.h"

#include "Window.h"

using namespace awt;

WindowCaption::WindowCaption(awt::Window* parent)
{
	this->dragged = false;
	this->parent = parent;
}

bool WindowCaption::isDragged() const
{
	return dragged;
}

bool WindowCaption::drag(const sf::Event e)
{
	switch (e.type)
	{
		case sf::Event::MouseButtonPressed:
			{
				if (e.mouseButton.button == sf::Mouse::Left)
				{
					sf::Vector2i cursor(e.mouseButton.x, e.mouseButton.y);

					if (getZone().contains(cursor))
					{
						dragLocation = cursor;
						dragged = true;
					}
				}
			}
			break;
		case sf::Event::MouseButtonReleased:
			{
				if (e.mouseButton.button == sf::Mouse::Left)
				{
					dragged = false;
				}
			}
			break;
		case sf::Event::MouseMoved:
		{
			if (isDragged())
			{
				sf::Vector2i npos = sf::Mouse::getPosition() - sf::Vector2i(dragLocation);
				parent->setLocation(awt::Point<int>(npos.x, npos.y));
				return true;
			}
		}
		break;
		default:
			break;
	}

	return false;
}

void WindowCaption::setZone(const sf::IntRect zone)
{
	this->zone = zone;
}

sf::IntRect WindowCaption::getZone() const
{
	return zone;
}
