#include "FlowLayout.h"

#include "Container.h"

awt::FlowLayout::FlowLayout()
{
	hgap = 5;
	vgap = 5;
}

awt::FlowLayout::FlowLayout(Alignment align)
{
	this->align = align;
	hgap = 5;
	vgap = 5;
}

awt::FlowLayout::FlowLayout(Alignment align, int hgap, int vgap)
{
	this->align = align;
	this->hgap = hgap;
	this->vgap = vgap;
}

void awt::FlowLayout::layoutContainer(awt::Container* container)
{
	// Extract component by line
	std::vector<std::vector<Component*>> componentsByLine = this->getComponentsByLine(container);
	// Give the height for each lines
	std::vector<int> linesHeight = this->getComponentsLineHeight(componentsByLine);

	int current_line_width = 0;
	int current_line_height = 0;
	int last_line_location = 0;

	for (size_t i = 0; i < componentsByLine.size(); i++)
	{
		current_line_width = hgap;
		int height = linesHeight[i];
		int ny = last_line_location + vgap;

		last_line_location += height + vgap;

		for (size_t j = 0; j < componentsByLine[i].size(); j++)
		{
			Component* component = componentsByLine[i][j];

			int nx = current_line_width;

			current_line_width += component->getSize().getWidth() + hgap;

			component->setSize(awt::Dimension<int>(component->getSize().getWidth(), component->getSize().getHeight()));
			component->setLocation(awt::Point<int>(nx, ny + height / 2 - component->getSize().getHeight() / 2));
		}
	}
}

awt::Dimension<int> awt::FlowLayout::minimumLayoutSize(Container* parent)
{
	int current_line_width = 0;
	int current_line_height = 0;

	int y = 0;
	int x = 0;

	for (auto& c : parent->getComponents())
	{
		int new_width = current_line_width + c->getMinimumSize().getWidth();
		int new_height = c->getSize().getHeight();

		if (new_height > current_line_height)
			current_line_height = new_height;

		if (new_width > parent->getSize().getWidth()) {
			y++;
			current_line_width = 0;
		}

		current_line_width = new_width + hgap;
	}

	return Dimension<int>(parent->getSize().getWidth(), current_line_height);
}

awt::Dimension<int> awt::FlowLayout::preferredLayoutSize(Container * parent)
{
	// Extract component by line
	std::vector<std::vector<Component*>> componentsByLine = this->getComponentsByLine(parent);
	// Give the height for each lines
	std::vector<int> linesHeight = this->getComponentsLineHeight(componentsByLine);

	int current_line_width = 0;
	int current_line_height = 0;
	int last_line_location = 0;

	int max_height = 0;
	int max_width  = 0;

	for (size_t i = 0; i < componentsByLine.size(); i++)
	{
		current_line_width = hgap;
		int height = linesHeight[i];
		int ny = last_line_location + vgap;

		last_line_location += height + vgap;

		for (size_t j = 0; j < componentsByLine[i].size(); j++)
		{
			Component* component = componentsByLine[i][j];

			int nx = current_line_width;

			current_line_width += component->getSize().getWidth() + hgap;
		}

		if (current_line_width > max_width)
			max_width = current_line_width;

		max_height = last_line_location;
	}

	return Dimension<int>(max_width, max_height);
}

std::vector<std::vector<awt::Component*>> awt::FlowLayout::getComponentsByLine(awt::Container* container)
{
	std::vector<std::vector<awt::Component*>> out;
	out.push_back(std::vector<awt::Component*>());
	
	int current_line_width = 0;

	for (size_t i = 0; i < container->getComponentCount(); i++)
	{
		Component* component = container->getComponent(i);

		current_line_width += component->getSize().getWidth() + hgap;

		if (current_line_width > container->getSize().getWidth() - 2 * hgap) {
			current_line_width = 0;
			out.push_back(std::vector<awt::Component*>());
		}

		out.back().push_back(component);

	}

	return out;
}

std::vector<int> awt::FlowLayout::getComponentsLineHeight(std::vector<std::vector<awt::Component*>>& componentsLine)
{
	std::vector<int> out;

	for (auto components : componentsLine)
	{
		int max_height = 0;

		for (auto component : components)
			if (component->getSize().getHeight() > max_height)
				max_height = component->getSize().getHeight();

		out.push_back(max_height);
	}

	return out;
}
