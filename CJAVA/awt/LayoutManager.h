#pragma once

#include <vector>

#include "../lang/Object.h"

#include "Point.h"
#include "Dimension.h"
#include "Component.h"

namespace awt
{
	/**
	 * A component is an object having a graphical representation that can be displayed on the screen and that can interact with the user. Examples of components are the buttons, checkboxes, and scrollbars of a typical graphical user interface.
	 */
	__interface ILayoutManager
	{
		public:

			/**
			 * Lays out the specified container.
			 */
			void layoutContainer(awt::Container* container) = 0;

			/**
			 * Calculates the minimum size dimensions for the 
			 * specified container, given the components it contains.
			 */
			Dimension<int> minimumLayoutSize(Container* parent) = 0;

			/**
			 * Calculates the preferred size dimensions for the 
			 * specified container, given the components it contains.
			 */
			Dimension<int> preferredLayoutSize(Container* parent) = 0;
	};
}