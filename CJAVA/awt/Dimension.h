#pragma once

#include "../lang/Object.h"

namespace awt
{
	/**
	 * A point representing a location in (x,y) 
	 * coordinate space, specified in integer precision.
	 */
	template < typename PrimType >
	class Dimension : public lang::Object
	{
		public:

			Dimension() {
				w = static_cast<PrimType>(0);
				h = static_cast<PrimType>(0);
			}

			Dimension(PrimType w, PrimType h) {
				this->w = w;
				this->h = h;
			}

			template < typename OtherPrimType >
			Dimension(const Dimension< OtherPrimType>& p) {
				w = static_cast<PrimType>(p.getWidth());
				h = static_cast<PrimType>(p.getHeight());
			}

			/**
			 * Returns the widht of the dimension
			 * in precision given by template arg.
			 */
			PrimType getWidth() const {
				return w;
			}

			/**
			 * Returns the height of the dimension
			 * in precision given by template arg.
			 */
			PrimType getHeight() const {
				return h;
			}

		private:

			PrimType w;
			PrimType h;
	};
}