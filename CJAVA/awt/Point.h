#pragma once

#include "../lang/Object.h"

namespace awt
{
	/**
	 * A point representing a location in (x,y) 
	 * coordinate space, specified in integer precision.
	 */
	template < typename PrimType >
	class Point : public lang::Object
	{
		public:

			Point() {
				x = static_cast<PrimType>(0);
				y = static_cast<PrimType>(0);
			}

			Point(PrimType x, PrimType y) {
				this->x = x;
				this->y = y;
			}

			template < typename OtherPrimType >
			Point(const Point< OtherPrimType>& p) {
				x = static_cast<PrimType>(p.getX());
				y = static_cast<PrimType>(p.getY());
			}

			/**
			 * Creates and returns a copy of this object.
			 */
			virtual Object::PTR clone() {
				return std::make_shared<Point<int>>(x, y);
			}

			/**
			 * Returns the X coordinate of this Point2D 
			 * in precision given by template arg.
			 */
			PrimType getX() const {
				return x;
			}

			/**
			 * Returns the Y coordinate of this Point2D
			 * in precision given by template arg.
			 */
			PrimType getY() const {
				return y;
			}

		private:

			PrimType x;
			PrimType y;
	};
}