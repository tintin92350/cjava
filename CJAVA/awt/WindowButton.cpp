#include "WindowButton.h"

#include <SFML/Math.hpp>

WindowButton::WindowButton()
{
	onCreate();
}

WindowButton::WindowButton(const sf::Vector2f position, const sf::Color color)
{
	onCreate();
	this->setFillColor(color);
}

void WindowButton::setFillColor(const sf::Color color)
{
	shape.setFillColor(color);
}

void WindowButton::handleEvents(sf::Event & event)
{
	if (event.type == sf::Event::MouseMoved)
	{
		sf::Vector2f cursor((float)event.mouseMove.x, (float)event.mouseMove.y);
		sf::Vector2f button = getPosition() + sf::Vector2f(shape.getRadius(), shape.getRadius());

		if (sf::distance(cursor, button) <= shape.getRadius())
			current_state = State::HOVER;
		else
			current_state = State::NORMAL;
	}
	else if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left && current_state == State::HOVER)
	{
		current_state = State::CLICKED;
	}
	else if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left && current_state == State::CLICKED)
	{
		current_state = State::NORMAL;

		if (_function)
			_function();
	}
}

void WindowButton::bindFunction(std::function<void()> _function)
{
	this->_function = _function;
}

int WindowButton::getCurrentState() const
{
	return current_state;
}

void WindowButton::onCreate()
{
	float radius = 6.0f;

	shape.setFillColor(sf::Color::White);
	shape.setRadius(radius);
	shape.setPointCount(300);

	current_state = State::NORMAL;
}
