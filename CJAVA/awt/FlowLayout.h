#pragma once

#include <vector>

#include "../lang/Object.h"

#include "Point.h"
#include "Dimension.h"
#include "Component.h"
#include "Insets.h"

#include "LayoutManager.h"

namespace awt
{
	/**
	 * A flow layout arranges components in a directional flow, much like 
	 * lines of text in a paragraph. The flow direction is determined by the 
	 * container's componentOrientation property and may be one of two values: 
	 */
	class FlowLayout : public lang::Object, public ILayoutManager
	{
		public:

			enum class Alignment {
				CENTER,
				LEFT,
				RIGHT
			};

			/**
			 * Constructs a new FlowLayout with a centered alignment 
			 * and a default 5-unit horizontal and vertical gap.
			 */
			FlowLayout();

			/**
			 * Constructs a new FlowLayout with the specified alignment 
			 * and a default 5-unit horizontal and vertical gap
			 */
			FlowLayout(Alignment align);

			/**
			 * Creates a new flow layout manager with the indicated
			 * alignment and the indicated horizontal and vertical gaps.
			 */
			FlowLayout(Alignment align, int hgap, int vgap);


			/**
			 * Lays out the specified container.
			 */
			void layoutContainer(awt::Container* container);

			/**
			 * Calculates the minimum size dimensions for the 
			 * specified container, given the components it contains.
			 */
			Dimension<int> minimumLayoutSize(Container* parent);

			/**
			 * Calculates the preferred size dimensions for the 
			 * specified container, given the components it contains.
			 */
			Dimension<int> preferredLayoutSize(Container *parent);

		protected:

			/**
			 * Cut the components by lines following their width
			 * and container size
			 */
			std::vector<std::vector<awt::Component*>> getComponentsByLine(awt::Container* container);

			/**
			 * Returns the greates height for each line
			 */
			std::vector<int> getComponentsLineHeight(std::vector<std::vector<awt::Component*>> & componentsLine);

			/**
			 * horizontal and vertical gaps
			 */
			int hgap;
			int vgap;
			Alignment align;

	};
}